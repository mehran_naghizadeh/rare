import EmberObject from '@ember/object';

export default EmberObject.extend({
    title: '',
    band: null,
    rating: 0,
});