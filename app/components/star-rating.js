import { computed } from '@ember/object';
import Component    from '@ember/component';

export default Component.extend({

    tagName: 'div',
    classNames: ['rating-panel'],

    rating:     0,
    maxRating:  5,

    item:       null,
    onclick()   {},

    stars: computed('rating', 'maxRating', function(){
        let stars = [];
        for (let i = 1; i <= this.maxRating; ++i)
            stars.push({rating: i, isFull: i <= this.rating});
        return stars;
    }),

    actions: {
        setRating: function(value) {
            this.item.set('rating', value);
        }
    }
}); 
